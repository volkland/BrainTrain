package main

import (
	"BrainTrain/app/route"
	"BrainTrain/config"
	"fmt"
	"log"
	"net/http"
)

func main() {
	mux := http.NewServeMux()
	route.App(mux)

	// Start the server
	port := config.Config.Port
	log.Printf("Listening at http://localhost:%d\n", port)
	err := http.ListenAndServe(fmt.Sprintf(":%d", port), mux)

	// Since ListenAndServe is running on the main thread, stuff
	// after it won't be called before the server is dead.
	if err != nil {
		log.Fatalln("Error starting server at :3000", err.Error())
	} else {
		log.Fatalln("Server stopped without an error.")
	}
}
