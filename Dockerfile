## The Builder
FROM golang:alpine as builder

# Get Dependencies
RUN apk add --no-cache git mercurial
RUN go get -u -v github.com/leesper/couchdb-golang && \
    go get -u gopkg.in/russross/blackfriday.v2 && \
    go get -u github.com/gorilla/sessions && \
    go get -u golang.org/x/crypto/...

## Build the app
RUN mkdir /go/src/BrainTrain 
ADD . /go/src/BrainTrain/
WORKDIR /go/src/BrainTrain/ 
RUN go build ./...
RUN go build -o main main.go

## Running Application

FROM alpine
RUN adduser -S -D -H -h /app appuser
USER appuser
COPY --from=builder /go/src/BrainTrain/main /app/

COPY . /app

WORKDIR /app
CMD ["./main"]