package model

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"reflect"
)

type KarteiCategory struct {
	Category     string
	CategoryId   int
	Karteien     []Kartei
	ShowExtended bool
}

func KarteienGetAll() ([]map[string]interface{}, error) {
	allKarteiens, err := BtDB.QueryJSON(`{	"selector": {"type": {"$eq": "Kartei"}}	}`)
	if err != nil {
		return nil, err
	} else {
		return allKarteiens, nil
	}
}

func KarteienGetByCat(kat KarteiCategoryTitle) ([]Kartei, error) {

	var returnArray []Kartei
	for _, secondLevel := range kat.SecondLevel {
		q := fmt.Sprintf(`{ "selector": { "type": "Kartei", "categoryId": %d, "public": true } }`, secondLevel.Id)

		mapstringinterface, err := BtDB.QueryJSON(q)
		if err != nil {
			return nil, err
		}

		for _, msi := range mapstringinterface {
			k := &Kartei{}

			byteBlob, _ := json.Marshal(msi)
			err := json.Unmarshal(byteBlob, &k)
			if err != nil {
				return nil, err
			}

			karteien, err := KartenGetByKarteiId(k.Id, false)
			if err != nil {
				return nil, err
			}
			k.CountKarten = len(karteien)

			k.Category = secondLevel.Title
			returnArray = append(returnArray, *k)
		}
	}
	return returnArray, nil
}

func KarteienGetByOwner(u User) ([]Kartei, error) {
	q := fmt.Sprintf(`{ "selector": { "type": "Kartei", "owner": "%s" } }`, u.Id)

	mapstringinterface, err := BtDB.QueryJSON(q)
	if err != nil {
		return nil, err
	}

	var returnArray []Kartei
	for _, msi := range mapstringinterface {
		k := &Kartei{}

		byteBlob, _ := json.Marshal(msi)
		err := json.Unmarshal(byteBlob, &k)
		if err != nil {
			return nil, err
		}
		k.Category = GetKarteiCategoryNameBySecondLevelId(k.CategoryId)

		karteien, err := KartenGetByKarteiId(k.Id, false)
		if err != nil {
			return nil, err
		}
		k.CountKarten = len(karteien)

		returnArray = append(returnArray, *k)
	}
	return returnArray, nil
}

func KarteiGetById(id string) (Kartei, error) {
	mapstringinterface, err := BtDB.Get(id, nil)

	// Keine da.
	if err != nil {
		return Kartei{}, err
	}

	k := &Kartei{}

	byteBlob, _ := json.Marshal(mapstringinterface)
	err = json.Unmarshal(byteBlob, &k)
	if err != nil {
		return Kartei{}, err
	}

	k.Category = GetKarteiCategoryNameBySecondLevelId(k.CategoryId)
	k.OwnerName = UserGetById(k.Owner).Name
	if k.OwnerName == "" {
		k.OwnerName = "Unbekannt, Kartei zu alt."
	}

	return *k, nil
}

func (k Kartei) toMap() map[string]interface{} {
	var doc map[string]interface{}
	kJSON, err := json.Marshal(k)
	if err != nil {
		log.Println(err.Error())
	}
	if err = json.Unmarshal(kJSON, &doc); err != nil {
		log.Println(err.Error())
	}

	return doc
}

func (s *Kartei) fillStruct(m map[string]interface{}) error {
	for k, v := range m {
		err := setField(s, k, v)
		if err != nil {
			return err
		}
	}
	return nil
}
func setField(obj interface{}, name string, value interface{}) error {
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(name)

	if !structFieldValue.IsValid() {
		return fmt.Errorf("No such field: %s in obj", name)
	}

	if !structFieldValue.CanSet() {
		return fmt.Errorf("Cannot set %s field value", name)
	}

	structFieldType := structFieldValue.Type()
	val := reflect.ValueOf(value)
	if structFieldType != val.Type() {
		return errors.New("Provided value type didn't match obj field type")
	}

	structFieldValue.Set(val)
	return nil
}

type KarteiCategoryTitle struct {
	Id          int
	Title       string
	SecondLevel []KarteiCategoryTitle
}

func CategoryTitles() []KarteiCategoryTitle {
	return []KarteiCategoryTitle{
		KarteiCategoryTitle{
			Id:    71,
			Title: "Naturwissenschaften",
			SecondLevel: []KarteiCategoryTitle{
				KarteiCategoryTitle{Id: 2, Title: "Biologie"},
				KarteiCategoryTitle{Id: 3, Title: "Chemie"},
				KarteiCategoryTitle{Id: 4, Title: "Elektrotechnik"},
				KarteiCategoryTitle{Id: 5, Title: "Informatik"},
				KarteiCategoryTitle{Id: 6, Title: "Mathematik"},
				KarteiCategoryTitle{Id: 7, Title: "Medizin"},
				KarteiCategoryTitle{Id: 8, Title: "Naturkunde"},
				KarteiCategoryTitle{Id: 9, Title: "Physik"},
				KarteiCategoryTitle{Id: 10, Title: "Sonstige"},
			},
		},
		KarteiCategoryTitle{
			Id:    72,
			Title: "Sprachen",
			SecondLevel: []KarteiCategoryTitle{
				KarteiCategoryTitle{Id: 21, Title: "Chinesisch"},
				KarteiCategoryTitle{Id: 22, Title: "Deutsch"},
				KarteiCategoryTitle{Id: 23, Title: "Englisch"},
				KarteiCategoryTitle{Id: 24, Title: "Französisch"},
				KarteiCategoryTitle{Id: 25, Title: "Griechisch"},
				KarteiCategoryTitle{Id: 26, Title: "Italienisch"},
				KarteiCategoryTitle{Id: 27, Title: "Latein"},
				KarteiCategoryTitle{Id: 28, Title: "Russisch"},
				KarteiCategoryTitle{Id: 29, Title: "Sonstiges"},
			},
		},
		KarteiCategoryTitle{
			Id:    73,
			Title: "Gesellschaft",
			SecondLevel: []KarteiCategoryTitle{
				KarteiCategoryTitle{Id: 30, Title: "Ethik"},
				KarteiCategoryTitle{Id: 31, Title: "Geschichte"},
				KarteiCategoryTitle{Id: 32, Title: "Literatur"},
				KarteiCategoryTitle{Id: 33, Title: "Musik"},
				KarteiCategoryTitle{Id: 34, Title: "Politik"},
				KarteiCategoryTitle{Id: 35, Title: "Recht"},
				KarteiCategoryTitle{Id: 36, Title: "Soziales"},
				KarteiCategoryTitle{Id: 37, Title: "Sport"},
				KarteiCategoryTitle{Id: 38, Title: "Verkehrskunde"},
				KarteiCategoryTitle{Id: 39, Title: "Sonstiges"},
			},
		},
		KarteiCategoryTitle{
			Id:    74,
			Title: "Wirtschaft",
			SecondLevel: []KarteiCategoryTitle{
				KarteiCategoryTitle{Id: 40, Title: "BWL"},
				KarteiCategoryTitle{Id: 41, Title: "Finanzen"},
				KarteiCategoryTitle{Id: 42, Title: "Landwirtschaft"},
				KarteiCategoryTitle{Id: 43, Title: "Marketing"},
				KarteiCategoryTitle{Id: 44, Title: "VWL"},
				KarteiCategoryTitle{Id: 45, Title: "Sonstige"},
				KarteiCategoryTitle{Id: 46, Title: "Finanzen"},
				KarteiCategoryTitle{Id: 47, Title: "Landwirtschaft"},
				KarteiCategoryTitle{Id: 48, Title: "Marketing"},
				KarteiCategoryTitle{Id: 49, Title: "VWL"},
				KarteiCategoryTitle{Id: 50, Title: "Sonstiges"},
			},
		},
		KarteiCategoryTitle{
			Id:    75,
			Title: "Geisteswissenschaften",
			SecondLevel: []KarteiCategoryTitle{
				KarteiCategoryTitle{Id: 51, Title: "Kriminologie"},
				KarteiCategoryTitle{Id: 52, Title: "Philosophie"},
				KarteiCategoryTitle{Id: 53, Title: "Psychologie"},
				KarteiCategoryTitle{Id: 54, Title: "Pädagogik"},
				KarteiCategoryTitle{Id: 55, Title: "Theologie"},
				KarteiCategoryTitle{Id: 56, Title: "Sonstiges"},
			},
		},
	}
}

func GetKarteiCategoryNameBySecondLevelId(id int) string {
	for _, f := range CategoryTitles() {
		for _, s := range f.SecondLevel {
			if s.Id == id {
				return fmt.Sprintf("%s ▶ %s", f.Title, s.Title)
			}
		}
	}

	return "unbekannte Kategorie"
}

func KarteiSave(k map[string]interface{}) (string, string, error) {
	return BtDB.Save(k, nil)
}
func KarteiUpdate(id string, k map[string]interface{}) (string, string, error) {
	d, err := BtDB.Get(id, nil)
	if err != nil {
		return "", "", err
	}

	k["_rev"] = d["_rev"]

	err = BtDB.Set(id, k)
	if err != nil {
		return "", "", err
	}

	return id, "", nil
}
func KarteiDelete(id string) error {
	return BtDB.Delete(id)
}
