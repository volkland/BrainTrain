package config

import (
	"log"
	"os"
)

type Configuration struct {
	DbUrl string
	Port  int
}

var Config Configuration

func init() {
	configs := map[string]Configuration{
		"dev": Configuration{
			DbUrl: "http://localhost:5984/bt",
			Port:  3000,
		},
	}

	env := Env()
	var ok bool
	if Config, ok = configs[env]; ok {
		log.Printf("Loaded configuration `%s`.", env)
	} else {
		log.Printf("Unable to load config `%s`.\n", env)
	}
}

func Env() string {
	env := os.Getenv("CONFIG")
	if env == "" {
		env = "dev"
	}
	return env
}
