document.addEventListener("DOMContentLoaded", () => {
	// Get all "navbar-burger" elements
	const $navbarBurgers = Array.prototype.slice.call(
		document.querySelectorAll(".navbar-burger"),
		0
	);

	// Check if there are any navbar burgers
	if ($navbarBurgers.length > 0) {
		// Add a click event on each of them
		$navbarBurgers.forEach(el => {
			el.addEventListener("click", () => {
				// Get the target from the "data-target" attribute
				const target = el.dataset.target;
				const $target = document.getElementById(target);

				// Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
				el.classList.toggle("is-active");
				$target.classList.toggle("is-active");
			});
		});
	}

	// Hide side-nav
	const KEY = "hide-side-nav";

	/* What it could do */
	const toggleSidebar = () => {
		document
			.getElementById("logo")
			.parentNode.classList.toggle("is-hidden");
		document.getElementById("left-nav").classList.toggle("is-hidden");
	};

	/* On Boot, check for the localStorage item */
	if (localStorage.getItem(KEY) == "true") {
		toggleSidebar();
	}

	/* Also, create an eventListener for the click events */
	const $hideSideNav = document.getElementById("hide-side-nav");
	if ($hideSideNav !== null) {
		$hideSideNav.addEventListener("click", () => {
			const toggleBooleanString = x => (x == "true" ? "false" : "true");

			localStorage.setItem(
				KEY,
				toggleBooleanString(localStorage.getItem(KEY))
			);
			toggleSidebar();
		});
	}

	// Simple MD Editor
	const $textEditors = Array.prototype.slice.call(
		document.querySelectorAll(".simplemde"),
		0
	);
	simpleMDEs = {};
	$textEditors.forEach($doc => {
		simpleMDEs[$doc.id] = new SimpleMDE({
			element: $doc,
			hideIcons: ["guide", "fullscreen", "preview", "side-by-side"],
			minHeight: "200px",
			status: false,
			spellChecker: false
		});
	});

	// Learning: Karte umklappen
	const $answerButton = document.getElementById("answer-button");
	if ($answerButton !== null) {
		$answerButton.addEventListener("click", e => {
			e.preventDefault();
			document.getElementById("learning").classList.add("show-answer");
			document.getElementsByTagName("form")[0].scrollIntoView();
		});
	}

	// Delete Modal
	const $deleteButtons = document.querySelectorAll(".delete-button");
	if ($deleteButtons !== null) {
		$deleteButtons.forEach(e => {
			e.addEventListener("click", () => {
				document
					.getElementById(e.dataset.target)
					.classList.add("is-active");
			});
		});
	}
	const $stayButton = Array.prototype.slice.call(
		document.querySelectorAll(".delete-stay"),
		0
	);
	$stayButton.forEach($btn => {
		if ($btn !== null) {
			$btn.addEventListener("click", () => {
				document.querySelectorAll(".modal").forEach(e => {
					e.classList.remove("is-active");
				});
			});
		}
	});

	// Kartei/View: Card chooser
	const $allCardDetails = Array.prototype.slice.call(
		document.querySelectorAll(".card-detail"),
		0
	);
	const $allCardChooser = Array.prototype.slice.call(
		document.querySelectorAll(".card-chooser"),
		0
	);

	$allCardChooser.forEach($cardChooser => {
		$cardChooser.addEventListener("click", () => {
			// Rechte Seite wechseln
			$allCardDetails.forEach($d => $d.classList.remove("is-active"));
			$allCardDetails
				.filter($d => $d.id == $cardChooser.dataset["id"])
				.forEach($d => {
					$d.classList.add("is-active");
				});
			Object.values(simpleMDEs).forEach(s => s.codemirror.refresh());

			// Linke Seite wechseln
			$allCardChooser.forEach($c => $c.classList.remove("is-active"));
			$cardChooser.classList.add("is-active");
		});
	});

	const karteSaveButtons = Array.from(
		document.getElementsByClassName("karte-save-button")
	);
	karteSaveButtons.forEach(b => {
		b.addEventListener("click", e => {
			karteId = b.dataset.id;
			karte = {
				karte: karteId,
				title: Array.from(
					document.getElementsByClassName("karte-input")
				).filter(
					e => e.dataset.karte == karteId && e.name == "title"
				)[0].value,
				question: simpleMDEs["question" + karteId].value(),
				answer: simpleMDEs["answer" + karteId].value()
			};
			// const fd = new FormData();

			// for (var k in karte) {
			// 	fd.append(k, karte[k]);
			// }

			console.log(karte);

			const url =
				"/kartei/saveKarte/" +
				window.location.href.substring(
					window.location.href.lastIndexOf("/") + 1
				);
			const rawResponse = fetch(url, {
				method: "POST",
				headers: {
					Accept: "application/json",
					//"Content-Type": "application/x-www-form-urlencoded"
					"Content-Type": "application/json"
				},
				body: JSON.stringify(karte)
			})
				.then(res => res.json())
				.then(j => {
					console.log(j);

					if (j.status == 500) {
						Array.from(
							document.getElementsByClassName("error-karte-save")
						)
							.filter(e => e.dataset.karte == karteId)
							.forEach(e => e.classList.remove("is-hidden"));
					} else if (j.status == 200) {
						if (j.reload) {
							window.location.reload();
						}
						Array.from(
							document.getElementsByClassName("error-karte-save")
						)
							.filter(e => e.dataset.karte == karteId)
							.forEach(e => e.classList.add("is-hidden"));
					}
				});
		});
	});

	const fileupload = document.getElementById("file");
	if (fileupload != null) {
		fileupload.addEventListener("change", e => {
			document.getElementById("filename").classList.remove("is-hidden");
			document.getElementById("filename").innerText =
				e.target.files[0].name;
			if (!e.target.files[0].type.startsWith("image/")) {
				document
					.getElementById("file-not-image")
					.classList.remove("is-hidden");
				document
					.getElementById("upload-button")
					.classList.add("is-hidden");
			} else {
				document
					.getElementById("file-not-image")
					.classList.add("is-hidden");
				document
					.getElementById("upload-button")
					.classList.remove("is-hidden");
			}
		});
	}

	const $catFilterSelect = document.getElementById("catFilter");
	if ($catFilterSelect !== null) {
		$catFilterSelect.addEventListener("change", e => {
			if (e.target.value < 0) {
				Array.from(
					document.getElementsByClassName("cat-sections")
				).forEach(e => e.classList.remove("is-hidden"));

				return;
			}
			e.preventDefault();
			Array.from(document.getElementsByClassName("cat-sections")).forEach(
				e => e.classList.add("is-hidden")
			);
			document
				.getElementById(e.target.value)
				.classList.remove("is-hidden");
		});
	}
});
