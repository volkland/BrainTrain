package template

import (
	"BrainTrain/app/model"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

type Head struct {
	LoggedInStatus bool
	Title          string
	NavActive      string
	User           model.User
	CountKarteien  int
}

func Render(w http.ResponseWriter, f string, data interface{}) {

	lp := filepath.Join(os.Getenv("GOPATH"), "src", "BrainTrain", "template", "default.html")
	fp := filepath.Join(os.Getenv("GOPATH"), "src", "BrainTrain", "template", f)

	tmpl, err := template.ParseFiles(lp, fp)
	if err != nil {
		// Log the detailed error
		log.Println(err.Error())
		http.Error(w, http.StatusText(500), 500)
		return
	}

	if err := tmpl.ExecuteTemplate(w, "layout", data); err != nil {
		log.Println(err.Error())
		http.Error(w, http.StatusText(500), 500)
	}
}
