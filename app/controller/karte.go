package controller

import (
	"BrainTrain/app/model"
	"BrainTrain/template"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func KarteLearnHandler(w http.ResponseWriter, r *http.Request) {
	cKarteienPublic := make(chan int)
	go getKarteienCount(cKarteienPublic, true)

	type Data struct {
		Head           template.Head
		Karte          model.Karte
		Progress       [5]int
		KarteiCategory model.KarteiCategory
		Kartei         model.Kartei
		Trays          [5]int
		Info           string
		ShowLearnCard  bool
	}
	var info string
	var nextCard model.Karte
	var kartei model.Kartei

	id := model.GetId(r, "/kartei/learn")
	if id == "" {
		w.WriteHeader(http.StatusNotFound)
		info = "Keine karteiID angegeben -- was magstn lernen?"
	} else {
		var err error
		kartei, err = model.KarteiGetById(id)
		if err != nil && err.Error() == "status 404 - not found" {
			w.WriteHeader(http.StatusNotFound)
			info = "Die angefragte Kartei existiert nicht."
		} else {

			if err != nil {
				log.Println("exported err:", err.Error())
				info = "Fehler bei der Datenbank-Abfrage. /KarteiGetById"
			} else {
				alleKarten, err := model.KartenGetByKarteiId(id, true)
				if err != nil {
					log.Println("exported err:", err.Error())
					info = "Fehler bei der Datenbank-Abrage. /KartenGetByKarteiId"
				}

				if len(alleKarten) == 0 {
					// http.Error(w, "Keine Karten vorhanden.", http.StatusOK)
					info = "Für diese Kartei gibt es keine Karten."
				} else {
					nextCard = model.KarteNextFromStack(alleKarten, r)
				}
			}
		}
	}

	data := Data{
		Head: template.Head{
			CountKarteien:  <-cKarteienPublic,
			LoggedInStatus: model.IsUserLoggedIn(r),
			User:           model.UserGetById(model.UserGetId(r)),
			Title:          "Lernen",
			NavActive:      "none",
		},
		Karte:         nextCard,
		Progress:      [5]int{3, 4, 7, 2, 9},
		Kartei:        kartei,
		Trays:         [5]int{4, 3, 2, 1, 0},
		Info:          info,
		ShowLearnCard: nextCard != model.Karte{},
	}

	template.Render(w, "kartei/learn.html", data)
}

func KarteSaveHandler(w http.ResponseWriter, r *http.Request) {
	var err error

	if r.Method != "POST" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	// err := r.ParseForm()
	// if err != nil {
	// 	log.Println(err.Error())
	// 	return
	// }

	karteiId := model.GetId(r, "/kartei/saveKarte")

	form := make(map[string]string)
	body, _ := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(body, &form)
	if err != nil {
		fmt.Fprintln(w, "{\"status\": 500, \"reload\": false}")
		log.Println("Parse Error", err.Error())
		return
	}

	k := map[string]interface{}{
		"type":     "Karte",
		"title":    form["title"],
		"Question": form["question"],
		"Answer":   form["answer"],
		"karteiId": karteiId,
	}

	reloadme := "false"

	kId := form["karte"]
	if kId == "-1" {
		err = model.KarteSave(k)
		reloadme = "true"

	} else {
		k["_id"] = kId
		err = model.KarteUpdate(kId, k)
	}

	if err != nil {
		log.Println("Save error", err.Error())
		fmt.Fprintf(w, "{\"status\": 500, \"reload\": false}")
	} else {
		fmt.Fprintf(w, "{\"status\": 200, \"reload\": %s}", reloadme)
	}
}

func KarteEditHandler(w http.ResponseWriter, r *http.Request) {
	cKarteienPublic := make(chan int)
	go getKarteienCount(cKarteienPublic, true)

	type Data struct {
		Head          template.Head
		Karten        []model.Karte
		ActiveKarteId string
		Kartei        model.Kartei
		Info          string
	}
	var info string
	var kartei model.Kartei
	var alleKarten []model.Karte

	id := model.GetId(r, "/kartei/editKarten")
	if id == "" {
		w.WriteHeader(http.StatusNotFound)
		info = "Keine karteiID angegeben -- was magstn lernen?"
	} else {
		var err error
		kartei, err = model.KarteiGetById(id)
		if err != nil && err.Error() == "status 404 - not found" {
			w.WriteHeader(http.StatusNotFound)
			info = "Die angefragte Kartei existiert nicht."
		} else {

			if err != nil {
				log.Println("exported err:", err.Error())
				info = "Fehler bei der Datenbank-Abfrage. /KarteiGetById"
			} else {
				alleKarten, err = model.KartenGetByKarteiId(id, false)
				if err != nil {
					log.Println("exported err:", err.Error())
					info = "Fehler bei der Datenbank-Abrage. /KartenGetByKarteiId"
				}

			}
		}
	}

	alleKarten = append(alleKarten, model.Karte{Id: "-1", Title: "Neu anlegen"})

	activeKarteId := ""
	activeKarteId = alleKarten[0].Id

	if kartei.Owner != model.UserGetId(r) {
		info = "Dir gehört diese Kartei nicht. Such Dir eine eigene!"
	}

	data := Data{
		Head: template.Head{
			CountKarteien:  <-cKarteienPublic,
			LoggedInStatus: model.IsUserLoggedIn(r),
			User:           model.UserGetById(model.UserGetId(r)),
			Title:          "Karte bearbeiten",
			NavActive:      "karteien-mine",
		},
		Karten:        alleKarten,
		ActiveKarteId: activeKarteId,
		Kartei:        kartei,
		Info:          info,
	}

	template.Render(w, "kartei/editKarten.html", data)
}

func KarteDeleteHandler(w http.ResponseWriter, r *http.Request) {
	id := model.GetId(r, "karte/delete")

	k, err := model.KarteGetById(id)
	if err != nil {
		log.Println(err.Error())
		fmt.Fprintf(w, "Irgendwas lief falsch, schau im Serverlog nach.")
		return
	}
	kartei, err := model.KarteiGetById(k.KarteiId)
	if err != nil {
		log.Println(err.Error())
		fmt.Fprintf(w, "Irgendwas lief falsch, schau im Serverlog nach.")
		return
	}

	if kartei.Owner != model.UserGetId(r) {
		fmt.Fprintf(w, "Dir gehört die Karte nicht. Schleich di")
		return
	}

	err = model.KarteDelete(id)
	if err != nil {
		log.Println(err.Error())
		fmt.Fprintf(w, "Irgendwas lief falsch, schau im Serverlog nach.")
		return
	}

	http.Redirect(w, r, fmt.Sprintf("/kartei/editKarten/%s", kartei.Id), http.StatusFound)
}
