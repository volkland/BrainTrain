package controller

import (
	"BrainTrain/app/model"
	"BrainTrain/template"
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"golang.org/x/crypto/bcrypt"
)

func UserRegisterHandler(w http.ResponseWriter, r *http.Request) {
	type Data struct {
		Head         template.Head
		ErrorGeneral string
		ErrorEmail   string
		ErrorUser    string
		ErrorPw      string
		U            map[string]interface{}
	}

	data := Data{
		Head: template.Head{
			LoggedInStatus: false,
			User:           model.UserGetById(model.UserGetId(r)),
			Title:          "Registrieren",
		},
		U: map[string]interface{}{"username": "", "email": ""},
	}

	template.Render(w, "user/register.html", data)
}
func UserRegisterPOSTHandler(w http.ResponseWriter, r *http.Request) {
	type Data struct {
		Head         template.Head
		ErrorGeneral string
		ErrorEmail   string
		ErrorUser    string
		ErrorPw      string
		U            map[string]interface{}
	}

	data := Data{
		Head: template.Head{
			LoggedInStatus: false,
			User:           model.UserGetById(model.UserGetId(r)),
			Title:          "Registrieren",
		},
	}

	u, err := model.UserFromRequest(r)
	data.U = u
	if err != nil {
		if strings.Contains(err.Error(), "Passwort") {
			data.ErrorPw = err.Error()
		}
	} else {
		errDB := model.SaveUser(u)
		if errDB != nil {
			if strings.Contains(errDB.Error(), "Email") {
				data.ErrorEmail = errDB.Error()
			} else if strings.Contains(errDB.Error(), "User") {
				data.ErrorUser = errDB.Error()
			} else {
				data.ErrorGeneral = errDB.Error()
			}
		}
	}

	if e(data.ErrorEmail) && e(data.ErrorGeneral) && e(data.ErrorPw) && e(data.ErrorUser) {
		http.Redirect(w, r, "/?message=RegisterSuccess", http.StatusFound)
	} else {
		template.Render(w, "user/register.html", data)
	}

}

func e(i string) bool {
	return i == ""
}

func UserProfileHandler(w http.ResponseWriter, r *http.Request) {
	cKarteienPublic := make(chan int)
	go getKarteienCount(cKarteienPublic, true)

	u := model.UserGetById(model.UserGetId(r))

	// logMe := false

	type Data struct {
		Head                       template.Head
		CountKarten                int
		CountKarteien              int
		ErrorEmail                 bool
		ErrorPasswordOld           bool
		ErrorPasswordNotMatching   bool
		ErrorPasswordNotChanged    bool
		ErrorPasswordNotGoodEnough bool
	}
	var data Data

	if r.Method == "POST" &&
		!(r.FormValue("email") == u.Email &&
			r.FormValue("password-old") == "" &&
			r.FormValue("password-1") == "" &&
			r.FormValue("password-2") == "") {
		// log.Println(1, data.ErrorEmail, u.Email)
		// Nur auf Duplette achten, wenn ich nicht diese Email habe
		if u.Email != r.FormValue("email") {

			users, err := model.UserGetAll()
			if err != nil {
				log.Println(err.Error())
			}
			for _, user := range users {
				// log.Println(2, data.ErrorEmail, user["email"], r.FormValue("email"), u.Email)
				if user["email"] == r.FormValue("email") {
					data.ErrorEmail = true
					// log.Println(3, data.ErrorEmail, user["email"], r.FormValue("email"), u.Email)
				}
			}
			if !data.ErrorEmail {
				// log.Println(4, data.ErrorEmail, u.Email)
				u.Email = r.FormValue("email")

				if strings.HasPrefix(u.AvatarUrl, "https://www.gravatar.com/avatar") {
					u.AvatarUrl = model.GravatarLink(u.Email)
				}
			}
			// log.Println(5, data.ErrorEmail, u.Email)
		}

		if !(r.FormValue("password-old") == "" &&
			r.FormValue("password-1") == "" &&
			r.FormValue("password-2") == "") {

			// Wenn -1 und -2 nicht gleich sind, mach nichts.
			if r.FormValue("password-1") != r.FormValue("password-2") {
				data.ErrorPasswordNotMatching = true
			} else {
				//sonst weiter

				passwordDB, _ := base64.StdEncoding.DecodeString(u.Password)
				err := bcrypt.CompareHashAndPassword(passwordDB, []byte(r.FormValue("password-old")))

				// Wenn Password-Old nicht das aus der Datenbank ist, mach nichts
				if err != nil {
					log.Println(err.Error())
					data.ErrorPasswordOld = true
				} else {
					//sonst weiter

					err = bcrypt.CompareHashAndPassword(passwordDB, []byte(r.FormValue("password-1")))
					// Passwort wurde nicht geändert.
					if err == nil {
						data.ErrorPasswordNotChanged = true
					} else {

						// Hier: Passwort ändern, wenn sich was geändert hat und keine Probleme

						pw_bytes, err := bcrypt.GenerateFromPassword([]byte(r.FormValue("password-1")), 14)

						// log.Println(r.FormValue("password-1"))
						// log.Println(base64.StdEncoding.EncodeToString(pw_bytes))

						if err != nil {
							log.Println(err.Error())
							data.ErrorPasswordNotGoodEnough = true
						} else {
							u.Password = base64.StdEncoding.EncodeToString(pw_bytes)
						}
					}
				}
			}
		}

		// Abspeichern:
		if !(data.ErrorEmail ||
			data.ErrorPasswordOld ||
			data.ErrorPasswordNotMatching ||
			data.ErrorPasswordNotChanged ||
			data.ErrorPasswordNotGoodEnough) {

			// log.Println(7, data.ErrorEmail, u.Email)
			err := model.UserUpdate(u)
			if err == nil {
				http.Redirect(w, r, "/user/profile", http.StatusFound)
			}
		}
	}

	var karteienCount int
	var kartenCount int

	karteien, err := model.KarteienGetByOwner(u)
	if err != nil {
		log.Println(err.Error())
	} else {
		karteienCount = len(karteien)
		for _, k := range karteien {
			kartenCount += k.CountKarten
		}
	}

	data.Head = template.Head{
		LoggedInStatus: model.IsUserLoggedIn(r),
		User:           u,
		Title:          "Mein Profil",
		NavActive:      "my-profile",
		CountKarteien:  <-cKarteienPublic,
	}
	data.CountKarteien = karteienCount
	data.CountKarten = kartenCount

	template.Render(w, "user/profile.html", data)
}

func UserDeleteHandler(w http.ResponseWriter, r *http.Request) {
	uIdsession := model.UserGetId(r)
	uIdurl := model.GetId(r, "user/delete")

	if uIdsession != uIdurl {
		fmt.Fprintf(w, "Lösch dich selbst, ned andere!")
	}

	var wg sync.WaitGroup

	karteien, err := model.KarteienGetByOwner(model.UserGetById(uIdsession))
	if err != nil {
		log.Println(err.Error())
	}
	wg.Add(len(karteien))
	for _, kartei := range karteien {
		go func(kartei model.Kartei) {

			// Kartei löschen
			defer wg.Done()
			err = model.KarteiDelete(kartei.Id)
			if err != nil {
				log.Println(err.Error())
			}

			// Und die Karten löschen
			karten, err := model.KartenGetByKarteiId(kartei.Id, false)
			if err != nil {
				log.Println(err.Error())
			}

			wg.Add(len(karten))
			for _, k := range karten {
				go func(k model.Karte) {
					defer wg.Done()
					err := model.KarteDelete(k.Id)
					if err != nil {
						log.Println(err.Error())
					}
				}(k)
			}
		}(kartei)
	}
	wg.Wait()

	err = model.UserDelete(uIdsession)
	if err != nil {
		log.Println(err.Error())
	}
	http.Redirect(w, r, "/user/logout", http.StatusFound)
}

func UserLoginHandler(w http.ResponseWriter, r *http.Request) {
	if model.UserTryLogin(r, w) {
		http.Redirect(w, r, "/", http.StatusFound)
	} else {
		http.Redirect(w, r, "/?message=loginFailed", http.StatusFound)
	}
}
func UserLogoutHandler(w http.ResponseWriter, r *http.Request) {
	err := model.UserTryLogout(r, w)
	if err != nil {
		log.Println("[Logout]:", err.Error())
	}
	http.Redirect(w, r, "/", http.StatusFound)
}

func UserUseGravatarHandler(w http.ResponseWriter, r *http.Request) {
	u := model.UserGetById(model.UserGetId(r))
	u.AvatarUrl = model.GravatarLink(u.Email)
	err := model.UserUpdate(u)
	if err != nil {
		fmt.Fprintln(w, err.Error())

	} else {
		http.Redirect(w, r, "/user/profile", http.StatusFound)
	}
}

func UserUploadImage(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}

	err := r.ParseMultipartForm(32 << 20) // Parse max 32MiB
	if err != nil {
		log.Println(err.Error())
	}
	file, handler, err := r.FormFile("uploadfile")
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()

	u := model.UserGetById(model.UserGetId(r))

	filename := fmt.Sprintf("%s%s", u.Id, filepath.Ext(handler.Filename))

	f, err := os.OpenFile("./static/upload/"+filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Println(err)
		return
	}
	defer f.Close()

	_, err = io.Copy(f, file)
	if err != nil {
		log.Println(err)
		return
	}

	u.AvatarUrl = "/static/upload/" + filename
	err = model.UserUpdate(u)
	if err != nil {
		fmt.Fprintln(w, err.Error())

	} else {
		http.Redirect(w, r, "/user/profile", http.StatusFound)
	}
}
