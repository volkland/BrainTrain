package controller

import (
	"BrainTrain/app/model"
	"BrainTrain/template"
	"fmt"
	htmlTemplate "html/template"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
)

func trim(s string) string {
	return strings.TrimSpace(s)
}

func KarteiNewHandler(w http.ResponseWriter, r *http.Request) {
	data := model.Kartei{}
	formErrors := make(map[string]string)

	if r.Method == "POST" {
		// 	`{
		// 	"type": "Kartei",
		// 	"title": "DONDE ESTA LA CATHEDRAL?",
		// 	"categoryId": 29,
		// 	"description": "Lorem ipsum dolor sit amet, ad quidem...",
		// 	"public": true,
		// 	"owner": "334a083755fd99b7a1481b961800233a"
		// }`,

		catId, errCatId := strconv.Atoi(r.FormValue("categoryId"))

		if errCatId != nil || catId < 2 || catId > 75 {
			formErrors["catId"] = "Die Kategorie wirft einen Fehler."
		}

		if trim(r.FormValue("title")) == "" {
			formErrors["title"] = "Der Titel darf nicht leer sein."
		}

		m := map[string]interface{}{
			"type":        "Kartei",
			"title":       trim(r.FormValue("title")),
			"categoryId":  catId,
			"description": trim(r.FormValue("description")),
			"public":      r.FormValue("public") == "public",
			"owner":       model.UserGetId(r),
		}

		if len(formErrors) == 0 {
			//saven gehen
			id, _, err := model.KarteiSave(m)
			if err != nil {
				formErrors["saveError"] = err.Error()
			} else {
				http.Redirect(w, r, fmt.Sprintf("/kartei/edit/%s", id), http.StatusFound)
			}
		}

	}

	karteiForm(w, r, "Neuer Karteikasten", data, true, htmlTemplate.HTML(""), formErrors)
}

func KarteiEditHandler(w http.ResponseWriter, r *http.Request) {
	id := model.GetId(r, "kartei/edit")
	if id == "" {
		KarteiNewHandler(w, r)
		return
	}
	data, err := model.KarteiGetById(id)
	if err != nil {
		log.Println("[/kartei/edit][KarteiEditHandler(", id, ")] failed:", err.Error())
	}

	var info htmlTemplate.HTML
	if (data == model.Kartei{}) {
		info = htmlTemplate.HTML(fmt.Sprintf("Die Kartei mit dem Identifier <code>%s</code> wurde nicht gefunden. <a href=\"/kartei/new\">Neu anlegen</a>?", id))
	}

	formErrors := make(map[string]string)

	if data.Owner != model.UserGetId(r) {
		info = htmlTemplate.HTML("Diese Karte gehört Dir nicht. Bitte geh weg.")
	}

	if r.Method == "POST" {
		// 	`{
		// 	"type": "Kartei",
		// 	"title": "DONDE ESTA LA CATHEDRAL?",
		// 	"categoryId": 29,
		// 	"description": "Lorem ipsum dolor sit amet, ad quidem...",
		// 	"public": true,
		// 	"owner": "334a083755fd99b7a1481b961800233a"
		// }`,

		catId, errCatId := strconv.Atoi(r.FormValue("categoryId"))

		if errCatId != nil || catId < 2 || catId > 75 {
			formErrors["catId"] = "Die Kategorie wirft einen Fehler."
		}

		if trim(r.FormValue("title")) == "" {
			formErrors["title"] = "Der Titel darf nicht leer sein."
		}

		m := map[string]interface{}{
			"_id":         id,
			"type":        "Kartei",
			"title":       trim(r.FormValue("title")),
			"categoryId":  catId,
			"description": trim(r.FormValue("description")),
			"public":      r.FormValue("public") == "public",
			"owner":       model.UserGetId(r),
		}

		if len(formErrors) == 0 {
			//saven gehen
			_, _, err := model.KarteiUpdate(id, m)
			if err != nil {
				log.Println(err.Error())
				formErrors["saveError"] = err.Error()
			} else {
				http.Redirect(w, r, fmt.Sprintf("/kartei/edit/%s", id), http.StatusFound)
				info = htmlTemplate.HTML("Update erfolgreich.")
			}
		}

	}

	karteiForm(w, r, "Karteikasten bearbeiten", data, false, info, formErrors)
}

func karteiForm(w http.ResponseWriter, r *http.Request, title string, kartei model.Kartei, isNewData bool, info htmlTemplate.HTML, errors map[string]string) {
	cKarteienPublic := make(chan int)
	go getKarteienCount(cKarteienPublic, true)

	type Data struct {
		Head           template.Head
		Kartei         model.Kartei
		NewData        bool
		CategoriesList []model.KarteiCategoryTitle
		Info           htmlTemplate.HTML
		ErrorsList     map[string]string
	}

	data := Data{
		Head: template.Head{
			CountKarteien:  <-cKarteienPublic,
			LoggedInStatus: model.IsUserLoggedIn(r),
			User:           model.UserGetById(model.UserGetId(r)),
			Title:          title,
			NavActive:      "karteien-mine",
		},
		Kartei:         kartei,
		NewData:        isNewData,
		CategoriesList: model.CategoryTitles(),
		Info:           info,
		ErrorsList:     errors,
	}

	template.Render(w, "kartei/form.html", data)
}

func KarteiViewHandler(w http.ResponseWriter, r *http.Request) {
	cKarteienPublic := make(chan int)
	go getKarteienCount(cKarteienPublic, true)

	type Data struct {
		Head          template.Head
		Kartei        model.Kartei
		Karten        []model.Karte
		ActiveKarteId string
		Trays         [5]int
	}

	id := model.GetId(r, "/kartei/view")
	if id == "" {
		http.NotFound(w, r)
		return
	}
	kartei, err := model.KarteiGetById(id)
	if err != nil && err.Error() == "status 404 - not found" {
		http.NotFound(w, r)
		return
	}

	if err != nil {
		log.Println("exported err:", err.Error())
		http.Error(w, "Error reading data from the DB", http.StatusInternalServerError)
		return
	}

	karten, err := model.KartenGetByKarteiId(id, true)
	if err != nil {
		log.Println("exported err:", err.Error())
		http.Error(w, "Error reading data from the DB", http.StatusInternalServerError)
		return
	}

	activeKarteId := "no-card-found"
	if len(karten) > 0 {
		activeKarteId = karten[0].Id
	}

	data := Data{
		Head: template.Head{
			CountKarteien:  <-cKarteienPublic,
			LoggedInStatus: model.IsUserLoggedIn(r),
			User:           model.UserGetById(model.UserGetId(r)),
			Title:          "Kartei ansehen",
			NavActive:      "karteien",
		},
		Kartei:        kartei,
		Karten:        karten,
		ActiveKarteId: activeKarteId,
		Trays:         [5]int{4, 3, 2, 1, 0}, // Todo
	}

	template.Render(w, "kartei/view.html", data)
}

func KarteiDeleteHandler(w http.ResponseWriter, r *http.Request) {
	id := model.GetId(r, "kartei/delete")
	kartei, err := model.KarteiGetById(id)
	if err != nil {
		log.Println(err.Error())
		return
	}
	requestor := model.UserGetId(r)

	if kartei.Owner != requestor {
		http.Error(w, "Kartei Owner is not the requestor", http.StatusUnauthorized)
		return
	}
	err = model.KarteiDelete(id)
	if err != nil {
		log.Println(err.Error())
	}

	karten, err := model.KartenGetByKarteiId(id, false)
	if err != nil {
		log.Println(err.Error())
	}

	var wg sync.WaitGroup
	wg.Add(len(karten))
	for _, k := range karten {
		go func(k model.Karte) {
			defer wg.Done()
			err := model.KarteDelete(k.Id)
			if err != nil {
				log.Println(err.Error())
			}
		}(k)
	}
	wg.Wait()

	http.Redirect(w, r, "/kartei/mine", http.StatusFound)
}
