package model

import (
	"testing"
)

func TestUniqueCategoryIds(t *testing.T) {
	var used [100]bool

	for _, firstCategory := range CategoryTitles() {
		if used[firstCategory.Id] == true {
			t.Fatalf("Duplicate id: `%d`", firstCategory.Id)
		}
		used[firstCategory.Id] = true
		for _, secondLevelCategory := range firstCategory.SecondLevel {
			if used[secondLevelCategory.Id] == true {
				t.Fatalf("Duplicate id: `%d`", secondLevelCategory.Id)
			}
			used[secondLevelCategory.Id] = true
		}
	}
}
