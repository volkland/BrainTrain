package main

import (
	"BrainTrain/app/model"
	"encoding/json"
	"log"
	"os"
)

func main() {

	// Set btDB by accessing a random function in braintrain.go
	var _ model.User

	for _, j := range getContent() {
		var doc map[string]interface{}
		errJ := json.Unmarshal([]byte(j), &doc)
		if errJ != nil {
			log.Println(errJ.Error())
			os.Exit(1)
		}
		id, rev, err := model.BtDB.Save(doc, nil)
		if err != nil {
			log.Println(err.Error())
			os.Exit(1)
		}
		log.Printf("saved: _id := %s, _rev := %s", id, rev)
	}
}

func getContent() []string {
	return []string{
		`{
		"_id": "334a083755fd99b7a1481b961800233a",
		"avatarUrl": "https://www.gravatar.com/avatar/474382a14716095a4c8ecccd785e546f?s=200",
		"email": "tim@metaebene.me",
		"name": "Max Mustermann",
		"password": "JDJhJDE0JGk1UGhWTDZZNkdvYy53QlM4MU1hS09zemx2cWpSUksyUjdFbzlWV2JSV2svd1RWRzJ0RUVP",
		"type": "User",
		"username": "Max Mustermann"
	}`,
		`{
		"_id": "334a083755fd99b7a1481b96180065cb",
		"Answer": "Ein **Teilchen**.",
		"Question": "Was ist eine Welle?\n\nDas ist die Frage!",
		"karteiId": "334a083755fd99b7a1481b9618014ec6",
		"title": "Was ist eine Welle?",
		"type": "Karte"
	}`,
		`{
		"_id": "334a083755fd99b7a1481b9618014ec6",
		"categoryId": 9,
		"description": "Millikan ist unser Freund.",
		"owner": "334a083755fd99b7a1481b961800233a",
		"public": true,
		"title": "Licht in Wellen und Teilchen - Modelle und Versuche",
		"type": "Kartei"
	}`,
		`{
		"_id": "334a083755fd99b7a1481b96180159f8",
		"categoryId": 23,
		"description": "Hier gibt es eine unvollständige Auflistung über die wichtigsten unregelmäßigen Verben in der englischen Sprache.",
		"owner": "334a083755fd99b7a1481b961800233a",
		"public": false,
		"title": "Unregelmäßige Verben",
		"type": "Kartei"
	}`,
		`{
		"_id": "334a083755fd99b7a1481b96180170ad",
		"categoryId": 51,
		"description": "Und böse Gören noch dazu",
		"owner": "334a083755fd99b7a1481b961800233a",
		"public": true,
		"title": "Wie man böse Buben fängt.",
		"type": "Kartei"
	}`,
		`{
		"_id": "3a12b860652c1538c16cf6e44300a325",
		"Answer": "Das war ein Waschbär. Yeah!",
		"Question": "Fährten lesen muss gelernt sein.",
		"karteiId": "334a083755fd99b7a1481b96180170ad",
		"title": "Schritt 1: Finden!",
		"type": "Karte"
	}`,
		`{
		"_id": "3a12b860652c1538c16cf6e44300b480",
		"Answer": "oder nur einer spaßigen",
		"Question": "Vielleicht doch mit Antwort",
		"karteiId": "334a083755fd99b7a1481b96180170ad",
		"title": "Schritt 2: Auf die Lauer legen",
		"type": "Karte"
	}`,
		`{
		"_id": "3a12b860652c1538c16cf6e44300c0fe",
		"Answer": "**jeder**!",
		"Question": "Wer darf das?",
		"karteiId": "334a083755fd99b7a1481b96180170ad",
		"title": "Schritt 3: Fangen.",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e2210000f7c",
		"Answer": "Gnocci mit Specksoße",
		"Question": "Was ist Dein Lieblingsgericht?",
		"karteiId": "334a083755fd99b7a1481b96180170ad",
		"title": "Schritt 4: Vor Gericht bringen",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e22100019a9",
		"Answer": "Brot! Richtig deutsch.",
		"Question": "Wie heißt das Sprichwort? Täglich **Wasser und ...**",
		"karteiId": "334a083755fd99b7a1481b96180170ad",
		"title": "Schritt 5: Sicher verwahren",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e221002f583",
		"Answer": "put, put",
		"Question": "to put",
		"karteiId": "334a083755fd99b7a1481b96180159f8",
		"title": "Setzen, stellen, legen",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e221002fb33",
		"Answer": "read, read",
		"Question": "to read",
		"karteiId": "334a083755fd99b7a1481b96180159f8",
		"title": "Lesen",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e221002fb8c",
		"Answer": "bought, bought",
		"Question": "to buy",
		"karteiId": "334a083755fd99b7a1481b96180159f8",
		"title": "Kaufen",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e2210030643",
		"Answer": "went, gone",
		"Question": "to go",
		"karteiId": "334a083755fd99b7a1481b96180159f8",
		"title": "Gehen",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e221003071f",
		"Answer": "knew, known",
		"Question": "to know",
		"karteiId": "334a083755fd99b7a1481b96180159f8",
		"title": "Wissen",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e2210031535",
		"Answer": "spoke, spoken",
		"Question": "to speak",
		"karteiId": "334a083755fd99b7a1481b96180159f8",
		"title": "Sprechen",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e2210031f10",
		"Answer": "swam, swum",
		"Question": "to swim",
		"karteiId": "334a083755fd99b7a1481b96180159f8",
		"title": "Schwimmen",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e2210032d20",
		"Answer": "wrote, written",
		"Question": "to write",
		"karteiId": "334a083755fd99b7a1481b96180159f8",
		"title": "Schreiben",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e2210035d2a",
		"Answer": "Eine **Welle**.",
		"Question": "Was ist ein Teilchen?",
		"karteiId": "334a083755fd99b7a1481b9618014ec6",
		"title": "Was ist ein Teilchen?",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e2210036d49",
		"categoryId": 5,
		"description": "The Elders of the Internet know who I am?",
		"owner": "334a083755fd99b7a1481b961800233a",
		"public": true,
		"title": "Serienzitate",
		"type": "Kartei"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e2210036e82",
		"Answer": "\t0118 999 881 999 119 725 3",
		"Question": "Schnellere Response, besser aussehende Fahrer, alles unter einer Nummer!",
		"karteiId": "5bfffacdacd52a4973d82e2210036d49",
		"title": "Was ist die neue Notfallnummer?",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e22100373f6",
		"Answer": "Turning it off and on again?",
		"Question": "Have you tried...",
		"karteiId": "5bfffacdacd52a4973d82e2210036d49",
		"title": "Tipp bei Problemen",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e22100380eb",
		"Answer": "> Answer for you. Eight recipes for octopus. My grandmother gave me a family recipe before she died in a horrible way.",
		"Question": "> Question for you. What's better than octopus recipe? ",
		"karteiId": "5bfffacdacd52a4973d82e2210036d49",
		"title": "Seafood",
		"type": "Karte"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e2210038cf2",
		"categoryId": 7,
		"description": "THemenbeschreibung",
		"owner": "334a083755fd99b7a1481b961800233a",
		"public": true,
		"title": "Noch ein NatWi Thema",
		"type": "Kartei"
	}`,
		`{
		"_id": "5bfffacdacd52a4973d82e2210038dd3",
		"Answer": "bin voll der rapper",
		"Question": "yoyoyo",
		"karteiId": "5bfffacdacd52a4973d82e2210038cf2",
		"title": "Beispielfrage #1",
		"type": "Karte"
	}`,
	}
}
