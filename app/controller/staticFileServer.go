package controller

import (
	"net/http"
	"strings"
)

func StaticFileServer() http.Handler {
	// The static folder only contains static files.
	fs := http.FileServer(http.Dir("static"))
	return http.StripPrefix("/static/", hideStaticIndex(fs))
}
func hideStaticIndex(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if strings.HasSuffix(r.URL.Path, "/") || r.URL.Path == "" {
			http.NotFound(w, r)
			return
		}

		next.ServeHTTP(w, r)
	})
}
