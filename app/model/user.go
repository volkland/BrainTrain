package model

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"

	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"
)

var key []byte
var store *sessions.CookieStore

func init() {
	// Cookie management
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	key = make([]byte, 32)
	_, err := rand.Read(key)
	if err != nil {
		log.Fatalln("Error creating key:", err.Error())
	}
	store = sessions.NewCookieStore(key)
}

func IsUserLoggedIn(r *http.Request) bool {
	session, _ := store.Get(r, "session")
	auth, ok := session.Values["authenticated"].(bool)
	return auth && ok
}

func UserTryLogin(r *http.Request, w http.ResponseWriter) bool {
	// Get the user from the DB
	q := fmt.Sprintf(`{"selector": {"type": "User", "username": "%s"}}`, r.FormValue("username"))
	msi, err := BtDB.QueryJSON(q)
	if err != nil {
		log.Println("[UserTryLogin/2] db error:", err.Error())
	}
	if len(msi) != 1 {
		return false
	}

	user := User{
		Id:        msi[0]["_id"].(string),
		Name:      msi[0]["name"].(string),
		Email:     msi[0]["email"].(string),
		AvatarUrl: msi[0]["avatarUrl"].(string),
		Password:  msi[0]["password"].(string),
	}

	// Check for equality
	passwordDB, _ := base64.StdEncoding.DecodeString(user.Password)
	err = bcrypt.CompareHashAndPassword(passwordDB, []byte(r.FormValue("password")))
	if err == nil {
		session, _ := store.Get(r, "session")

		// Set user as authenticated
		userM, err := json.Marshal(user)
		if err != nil {
			log.Println(err.Error())
		}

		session.Values["user"] = userM
		session.Values["userId"] = user.Id
		session.Values["authenticated"] = true

		err = session.Save(r, w)
		if err != nil {
			log.Println(err.Error())
		}
		return true
	}
	return false
}

func UserGetId(r *http.Request) string {
	session, _ := store.Get(r, "session")
	if session.Values["userId"] == nil {
		return ""
	}
	return session.Values["userId"].(string)
}

func UserTryLogout(r *http.Request, w http.ResponseWriter) error {
	session, err := store.Get(r, "session")
	if err != nil {
		return err
	}
	session.Values["user"] = []byte("")
	session.Values["authenticated"] = false
	session.Values["userId"] = ""
	session.Save(r, w)
	return nil
}

func UserGetAll() ([]map[string]interface{}, error) {
	allUsers, err := BtDB.QueryJSON(`{	"selector": {"type": {"$eq": "User"}}	}`)
	if err != nil {
		return nil, err
	} else {
		return allUsers, nil
	}
}

func UserFromRequest(r *http.Request) (map[string]interface{}, error) {
	emptyMap := make(map[string]interface{})

	if !strings.EqualFold(r.FormValue("password-1"), r.FormValue("password-2")) {
		return emptyMap, fmt.Errorf("Passwort und 'Passwort wiederholen' stimmen nicht überein.")
	}

	pw_bytes, err := bcrypt.GenerateFromPassword([]byte(r.FormValue("password-1")), 14)
	if err != nil {
		return emptyMap, fmt.Errorf("Passwort erfüllt nicht die Anforderungen.")
	}

	u := map[string]interface{}{
		"type":      "User",
		"username":  r.FormValue("username"),
		"name":      r.FormValue("username"),
		"email":     r.FormValue("email"),
		"avatarUrl": GravatarLink(r.FormValue("email")),
		"password":  base64.StdEncoding.EncodeToString(pw_bytes),
	}

	return u, nil
}

func SaveUser(u map[string]interface{}) error {
	allUsers, _ := UserGetAll()
	for _, known := range allUsers {
		if known["email"] == u["email"] {
			return fmt.Errorf("Email-Adresse wird bereits verwendet.")
		}
		if known["username"] == u["username"] {
			return fmt.Errorf("Username wird bereits verwendet.")
		}

	}

	_, _, err := BtDB.Save(u, nil)

	return err
}

func UserUpdate(u User) error {
	d, err := BtDB.Get(u.Id, nil)
	if err != nil {
		return err
	}

	j, err := json.Marshal(u)
	if err != nil {
		return err
	}

	m := make(map[string]interface{})
	err = json.Unmarshal(j, &m)
	if err != nil {
		return err
	}

	m["_rev"] = d["_rev"]
	m["type"] = "User"
	m["username"] = m["name"]
	err = BtDB.Set(u.Id, m)
	if err != nil {
		return err
	}

	return nil
}

func GravatarLink(email string) string {
	cleanEmail := strings.ToLower(strings.TrimSpace(email))
	mdfive := md5.Sum([]byte(cleanEmail))
	return fmt.Sprintf("https://www.gravatar.com/avatar/%x?s=200", mdfive)
}

// At this time, the function only returns the values that are necassary for the navigation.
// You can add more fields later, but please don't remove the fields given.
func UserGetById(id string) User {
	msi, err := BtDB.Get(id, nil)

	if err != nil {
		log.Println(err.Error())
		return User{}
	}

	if msi["name"] == nil {
		return User{}
	}

	u := User{
		Id:        msi["_id"].(string),
		Name:      msi["name"].(string),
		AvatarUrl: msi["avatarUrl"].(string),
		Email:     msi["email"].(string),
		Password:  msi["password"].(string),
	}
	return u
}

func UserDelete(id string) error {
	return BtDB.Delete(id)
}
