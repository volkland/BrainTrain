package model

import (
	"BrainTrain/config"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"regexp"
	"time"

	couchdb "github.com/leesper/couchdb-golang"
)

// Data Structures
type Karte struct {
	Id         string `json:"_id"`
	Rev        string `json:"_rev"`
	Type       string `json:"type"`
	KarteiId   string `json:"karteiId"`
	Title      string `json:"title"`
	Question   template.HTML
	QuestionMD string `json:"Question"`
	Answer     template.HTML
	AnswerMD   string `json:"Answer"`
	Tray       int    // I don't know.

	couchdb.Document
}
type Kartei struct {
	Id          string `json:"_id"`
	Rev         string `json:"_rev"`
	Type        string `json:"type"`
	Title       string `json:"title"`
	Category    string //Retrievable from the Category Table
	CategoryId  int    `json:"categoryId"`
	CountKarten int    // View thing?
	Description string `json:"description"`
	Public      bool   `json:"public"`
	Progress    int    //
	Owner       string `json:"owner"`
	OwnerName   string

	couchdb.Document
}
type User struct {
	Id   string `json:"_id"`
	Rev  string `json:"_rev"`
	Type string `json:"type"`

	Name      string `json:"name"`
	Email     string `json:"email"`
	AvatarUrl string `json:"avatarUrl"`
	Password  string `json:"password"`

	couchdb.Document
}

var BtDB *couchdb.Database

func init() {
	var err error
	BtDB, err = couchdb.NewDatabase(config.Config.DbUrl)
	if err != nil {
		panic(err)
	}
	healthcheck(true)

	// Check every minute if the db is available. FROM: https://stackoverflow.com/a/16466581
	ticker := time.NewTicker(1 * time.Minute)
	Quit := make(chan bool)
	go func() {
		for {
			select {
			case <-ticker.C:
				healthcheck(false)
			case <-Quit:
				ticker.Stop()
				return
			}
		}
	}()

}
func healthcheck(logPositiveResult bool) {
	if err := BtDB.Available(); err != nil {
		log.Printf("Database `%s` is not available. Check if its online or change the config file.\n", config.Config.DbUrl)
	} else if logPositiveResult {
		log.Printf("Database `%s` is available.\n", config.Config.DbUrl)
	}
}

func GetId(r *http.Request, controller string) string {
	url := r.URL.Path

	re, _ := regexp.Compile(fmt.Sprintf("%s/([0-9a-fA-F]*)$", controller))
	values := re.FindStringSubmatch(url)
	if len(values) > 0 {
		return values[1]
	}
	return ""
}
