# BrainTrain

> Karteikarten-Trainer für Web-Programmierung 1

**Links:**

* [ILIAS: WEB-Root](https://ilias.th-koeln.de/ilias.php?ref_id=1253342&cmd=frameset&cmdClass=ilrepositorygui&cmdNode=u0&baseClass=ilrepositorygui)
* [ILIAS: Semesterprojekt](https://ilias.th-koeln.de/ilias.php?ref_id=1253637&cmd=view&cmdClass=ilrepositorygui&cmdNode=u0&baseClass=ilrepositorygui)
* [ILIAS: Wireframes](https://ilias.th-koeln.de/goto.php?target=file_1253640_download&client_id=ILIAS_FH_Koeln)

## Fill database with dummy data

1. Create the database: `curl -X PUT http://admin:admin@127.0.0.1:5984/bt`
2. Run the fill script from the project dir: `go run ./fillDB/main.go`
3. A number of `_id` and `_rev` tuples should be returned.

## Dependencies:
* CouchDB-Adapter: `go get -u -v github.com/leesper/couchdb-golang`
* Markdown-Parser: `go get -u gopkg.in/russross/blackfriday.v2`

# Windows:

Build: `go build ./...; go build main.go; .\main.exe`

Test: `go test -v ./...`

Run Benchmark Tests: `go test -benchmem -run=^$ BrainTrain\app\controller -bench=BenchmarkIndexHandler`

Launch DB Server: `docker run -p 5984:5984 -v "$(pwd)/database":/opt/couchdb/data -it --rm couchdb`

# Linux with service file
Build: `go build ./... && go build main.go && sudo service braintrain restart`

## Deployment

1. Log into the uni server
2. Kill the server: `systemctl stop braintrain`
3. Pull the latest information via git. SSH pw is the campusId pw
4. Build the service and let it run again: `go build main.go`
5. Start the server again: `systemctl start braintrain`
6. If the application name changes, you need to reconfigure the service file at `/etc/systemd/system/braintrain.service`
6. If things dont work, try restarting apache: `sudo systemctl apache2 restart`

## Service file `/etc/systemd/system/braintrain.service`

```
[Unit]
Description=BrainTrain service
After=syslog.target
After=network.target

[Service]
Type=simple

Restart=on-failure
RestartSec=10
startLimitIntervalSec=60
WorkingDirectory=/home/ubuntu/go/src/BrainTrain
ExecStart=/home/ubuntu/go/src/BrainTrain/main

# make sure log directory exists and owned by syslog
PermissionsStartOnly=true
ExecStartPre=/bin/mkdir -p /var/log/BrainTrain
ExecStartPre=/bin/chown syslog:adm /var/log/BrainTrain
ExecStartPre=/bin/chmod 755 /var/log/BrainTrain
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=BrainTrain
NonBlocking=true

[Install]
WantedBy=multi-user.target
```