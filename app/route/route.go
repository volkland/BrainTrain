package route

import (
	"BrainTrain/app/controller"
	"BrainTrain/app/model"
	"net/http"
)

// Does not return anything but adds Routes as a side effect.
func App(m *http.ServeMux) {
	// Use a static file server for the static stuff.
	m.Handle("/static/", controller.StaticFileServer())

	m.HandleFunc("/", controller.IndexHandler)
	m.HandleFunc("/index", controller.IndexHandler)
	m.HandleFunc("/index.html", controller.IndexHandler)

	m.HandleFunc("/kartei/list", controller.KarteiListHandler)
	m.HandleFunc("/kartei/mine", Auth(controller.KarteiMineListHandler))
	m.HandleFunc("/kartei/view/", Auth(controller.KarteiViewHandler))
	m.HandleFunc("/kartei/new", Auth(controller.KarteiNewHandler))
	m.HandleFunc("/kartei/edit/", Auth(controller.KarteiEditHandler))
	m.HandleFunc("/kartei/editKarten/", Auth(controller.KarteEditHandler))
	m.HandleFunc("/kartei/saveKarte/", Auth(controller.KarteSaveHandler))
	m.HandleFunc("/karte/delete/", Auth(controller.KarteDeleteHandler))

	m.HandleFunc("/kartei/delete/", Auth(controller.KarteiDeleteHandler))
	m.HandleFunc("/kartei/learn/", Auth(controller.KarteLearnHandler))

	m.HandleFunc("/user/registerPost", controller.UserRegisterPOSTHandler)
	m.HandleFunc("/user/register", controller.UserRegisterHandler)
	m.HandleFunc("/user/profile", Auth(controller.UserProfileHandler))
	m.HandleFunc("/user/profile/useGravatar", Auth(controller.UserUseGravatarHandler))
	m.HandleFunc("/user/profile/uploadImage", Auth(controller.UserUploadImage))
	m.HandleFunc("/user/login", controller.UserLoginHandler)
	m.HandleFunc("/user/logout", controller.UserLogoutHandler)
	m.HandleFunc("/user/delete/", Auth(controller.UserDeleteHandler))
}

func Auth(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// Check if user is authenticated
		if !model.IsUserLoggedIn(r) {
			http.Redirect(w, r, "/index?message=notLoggedIn", http.StatusFound)
		} else {
			h(w, r)
		}
	}
}
