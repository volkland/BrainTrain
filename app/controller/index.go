package controller

import (
	"BrainTrain/app/model"
	"BrainTrain/template"
	"log"
	"net/http"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	// 404 Handling
	myURLs := []string{"/", "/index", "/index.html"}
	reqUrlUnkown := true
	for _, n := range myURLs {
		if r.URL.Path == n {
			reqUrlUnkown = false
		}
	}
	if reqUrlUnkown {
		http.NotFound(w, r)
		return
	}

	// Actual /index handling
	type Data struct {
		Head            template.Head
		CountUsers      int
		CountKarten     int
		CountKarteien   int
		NotLoggedIn     bool
		LoginFailed     bool
		RegisterSuccess bool
	}

	message := r.FormValue("message")

	cUsers := make(chan int)
	go getUserCount(cUsers)
	cKarten := make(chan int)
	go getKartenCount(cKarten)
	cKarteien := make(chan int)
	go getKarteienCount(cKarteien, false)

	cKarteienPublic := make(chan int)
	go getKarteienCount(cKarteienPublic, true)

	data := Data{
		Head: template.Head{
			CountKarteien:  <-cKarteienPublic,
			LoggedInStatus: model.IsUserLoggedIn(r),
			User:           model.UserGetById(model.UserGetId(r)),
			Title:          "",
			NavActive:      "home",
		},
		CountUsers:      <-cUsers,
		CountKarten:     <-cKarten,
		CountKarteien:   <-cKarteien,
		NotLoggedIn:     message == "notLoggedIn",
		LoginFailed:     message == "loginFailed",
		RegisterSuccess: message == "RegisterSuccess",
	}

	template.Render(w, "index.html", data)
}

func getUserCount(c chan int) {
	defer close(c)

	users, err := model.UserGetAll()
	if err != nil {
		log.Println("[/index][getUserCount]", err.Error())
	}
	c <- len(users)
}

func getKarteienCount(c chan int, publicOnly bool) {
	defer close(c)

	karteien, err := model.KarteienGetAll()
	if err != nil {
		log.Println("[/index][getKarteienCount]", err.Error())
	}

	if publicOnly {
		count := 0
		for _, k := range karteien {
			if k["public"].(bool) {
				count += 1
			}
		}
		c <- count
	} else {
		c <- len(karteien)
	}
}

func getKartenCount(c chan int) {
	defer close(c)

	karten, err := model.KartenGetAll()
	if err != nil {
		log.Println("[/index][getKartenCount]", err.Error())
	}
	c <- len(karten)
}
