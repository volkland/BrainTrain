package main

import (
	"BrainTrain/app/controller"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestAssetsFolderIs200ForFilesAnd404ForDirListings(t *testing.T) {
	log.SetOutput(ioutil.Discard)

	err := filepath.Walk("./static", func(path string, f os.FileInfo, err error) error {
		t.Run(path, func(t *testing.T) {
			if err != nil {
				t.Fatalf(err.Error())
				return
			}

			var appendSlash string = ""
			if f.IsDir() {
				appendSlash = "/"
			}
			url := fmt.Sprintf("/%s%s", strings.Replace(path, "\\", "/", -1), appendSlash)

			req, err := http.NewRequest("GET", url, nil)
			if err != nil {
				t.Fatal(err)
			}

			// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
			rr := httptest.NewRecorder()
			handler := http.Handler(controller.StaticFileServer())

			// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
			// directly and pass in our Request and ResponseRecorder.
			handler.ServeHTTP(rr, req)

			var expectedStatusCode int = 200

			if f.IsDir() { // Folders / Dir Listings should return a 404.
				expectedStatusCode = http.StatusNotFound
			}
			if status := rr.Code; status != expectedStatusCode {
				t.Errorf("handler for url `%s` returned wrong status code: got %v want %v",
					url, status, expectedStatusCode)
			}
		})

		return nil
	})
	if err != nil {
		t.Errorf(err.Error())
	}
}
