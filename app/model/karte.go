package model

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"math/rand"
	"net/http"

	"gopkg.in/russross/blackfriday.v2"
)

func KartenGetAll() ([]map[string]interface{}, error) {
	allKarten, err := BtDB.QueryJSON(`{	"selector": {"type": {"$eq": "Karte"}}	}`)
	if err != nil {
		return nil, err
	} else {
		return allKarten, nil
	}
}

func KarteGetById(id string) (Karte, error) {
	mapstringinterface, err := BtDB.Get(id, nil)

	// Keine da.
	if err != nil {
		return Karte{}, err
	}

	k := &Karte{}

	byteBlob, _ := json.Marshal(mapstringinterface)
	err = json.Unmarshal(byteBlob, &k)
	if err != nil {
		return Karte{}, err
	}

	return *k, nil
}

func KartenGetByKarteiId(id string, markedDown bool) ([]Karte, error) {
	q := fmt.Sprintf(`{"selector": {"type": "Karte","karteiId": "%s"}}`, id)
	allKartenMSI, err := BtDB.QueryJSON(q)
	if err != nil {
		return nil, err
	}

	var allKarten []Karte
	for _, karteMSI := range allKartenMSI {
		karte := Karte{}

		byteBlob, _ := json.Marshal(karteMSI)
		err = json.Unmarshal(byteBlob, &karte)
		if err != nil {
			return nil, err
		}

		// TODO: Add Tray depending on the user.
		if markedDown {
			karte.MarkdownMe()
		}
		allKarten = append(allKarten, karte)
	}
	return allKarten, nil

}

func KarteSave(k map[string]interface{}) error {
	_, _, err := BtDB.Save(k, nil)
	return err
}
func KarteUpdate(id string, k map[string]interface{}) error {
	d, err := BtDB.Get(id, nil)
	if err != nil {
		return err
	}

	k["_rev"] = d["_rev"]

	err = BtDB.Set(id, k)
	if err != nil {
		return err
	}

	return nil
}

func KarteDelete(id string) error {
	return BtDB.Delete(id)
}

// Here, we should implement wireframes 5.2, but this
// does not make sense before know how to learn properly.
// So for now, let's just take a random Item and take
// care that we don't ask the same stuff twice in a row.
func KarteNextFromStack(stack []Karte, r *http.Request) Karte {
	keys := r.URL.Query()
	lastCardId := keys.Get("lastCardId")

	if lastCardId == "" {
		return stack[0]
	}
	if len(stack) == 1 {
		return stack[0]
	}

	index := rand.Intn(len(stack))
	for stack[index].Id == lastCardId {
		index = rand.Intn(len(stack))
	}

	return stack[index]
}

func (k Karte) toMap() map[string]interface{} {
	var doc map[string]interface{}
	kJSON, err := json.Marshal(k)
	if err != nil {
		log.Println(err.Error())
	}
	if err = json.Unmarshal(kJSON, &doc); err != nil {
		log.Println(err.Error())
	}

	return doc
}

func (s *Karte) fillStruct(m map[string]interface{}) error {
	for k, v := range m {
		err := setField(s, k, v)
		if err != nil {
			return err
		}
	}
	return nil
}

func (k *Karte) MarkdownMe() {
	k.Question = template.HTML(blackfriday.Run([]byte(k.QuestionMD)))
	k.Answer = template.HTML(blackfriday.Run([]byte(k.AnswerMD)))
}
