package model

import (
	"net/http"
	"reflect"
	"testing"
)

func mustHttpRequest(r *http.Request, e error) *http.Request {
	if e != nil {
		panic(e)
	}
	return r
}

func TestKarteNextFromStack(t *testing.T) {
	type args struct {
		stack []Karte
		r     *http.Request
	}
	tests := []struct {
		name string
		args args
		want Karte
	}{
		{
			name: "Return the first item, if lCI is empty",
			args: args{
				stack: []Karte{Karte{Id: "0"}, Karte{Id: "1"}},
				r:     mustHttpRequest(http.NewRequest("GET", "/kartei/learn/abc", nil)),
			},
			want: Karte{Id: "0"},
		},
		{
			name: "Don't return the item which was asked before",
			args: args{
				stack: []Karte{Karte{Id: "0"}, Karte{Id: "1"}},
				r:     mustHttpRequest(http.NewRequest("GET", "/kartei/learn/abc?lastCardId=0", nil)),
			},
			want: Karte{Id: "1"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := KarteNextFromStack(tt.args.stack, tt.args.r); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("KarteNextFromStack() = %v, want %v", got, tt.want)
			}
		})
	}
}
