package controller

import (
	"fmt"
	"go/build"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func gopath() string {
	gopath := os.Getenv("GOPATH")
	if gopath == "" {
		gopath = build.Default.GOPATH
	}
	return gopath
}

func ServeTemplate(w http.ResponseWriter, r *http.Request) {
	requestPath := r.URL.Path

	if requestPath == "/" {
		requestPath = "/index.html"
	}
	// If the request path does not end with ".html", just assume it has.
	if !strings.HasSuffix(requestPath, ".html") {
		requestPath = fmt.Sprintf("%s.html", requestPath)
	}
	lp := filepath.Join(gopath(), "src", "BrainTrain", "template", "default.html")
	fp := filepath.Join(gopath(), "src", "BrainTrain", "template", filepath.Clean(requestPath))

	// Return a 404 if the template doesn't exist
	info, err := os.Stat(fp)
	if err != nil {
		if os.IsNotExist(err) {
			http.NotFound(w, r)
			log.Printf("404: '%s' not found.\n", requestPath)
			return
		}
	}

	// Return a 404 if the request is for a directory
	if info.IsDir() {
		http.NotFound(w, r)
		return
	}

	// Render the template,
	tmpl, err := template.ParseFiles(lp, fp)
	if err != nil {
		// Log the detailed error
		log.Println(err.Error())
		http.Error(w, http.StatusText(500), 500)
		return
	}
	if err := tmpl.ExecuteTemplate(w, "layout", nil); err != nil {
		log.Println(err.Error())
		http.Error(w, http.StatusText(500), 500)
	}
}
