package route

import (
	"bytes"
	"encoding/base64"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func createMultiplexer() *http.ServeMux {
	mux := http.NewServeMux()
	App(mux)

	return mux
}

func mustHttpRequest(r *http.Request, e error) *http.Request {
	if e != nil {
		panic(e)
	}
	return r
}

func TestIndexRedirection(t *testing.T) {

	reqSlash := mustHttpRequest(http.NewRequest("GET", "/", nil))
	reqIndexDotHtml := mustHttpRequest(http.NewRequest("GET", "/index.html", nil))
	reqIndexWithoutExtension := mustHttpRequest(http.NewRequest("GET", "/index", nil))

	rr := httptest.NewRecorder()
	mux := createMultiplexer()

	// Request /
	mux.ServeHTTP(rr, reqSlash)
	contentSlash, _ := ioutil.ReadAll(rr.Body)

	// Request /index.html
	mux.ServeHTTP(rr, reqIndexDotHtml)
	contentIndexDotHtml, _ := ioutil.ReadAll(rr.Body)

	// Request /index
	mux.ServeHTTP(rr, reqIndexWithoutExtension)
	contentIndexWithoutExtension, _ := ioutil.ReadAll(rr.Body)

	if !bytes.Equal(contentSlash, contentIndexDotHtml) {
		t.Fatalf("/ and /index.html did not result in the same response body.\n/: %s\n/index.html: %s", base64.StdEncoding.EncodeToString(contentSlash), base64.StdEncoding.EncodeToString(contentIndexDotHtml))
	}
	if !bytes.Equal(contentSlash, contentIndexWithoutExtension) {
		t.Fatalf("/ and /index did not result in the same response body.\n/: %s\n/index.html: %s", base64.StdEncoding.EncodeToString(contentSlash), base64.StdEncoding.EncodeToString(contentIndexWithoutExtension))
	}
	if !bytes.Equal(contentIndexDotHtml, contentIndexWithoutExtension) {
		t.Fatalf("/index.html and /index did not result in the same response body.\n/: %s\n/index.html: %s", base64.StdEncoding.EncodeToString(contentIndexDotHtml), base64.StdEncoding.EncodeToString(contentIndexWithoutExtension))
	}
}

func TestNotFound(t *testing.T) {
	log.SetOutput(ioutil.Discard)

	req := mustHttpRequest(http.NewRequest("GET", "/unknown-page.html", nil))

	// We create a ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := createMultiplexer()

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}
}
