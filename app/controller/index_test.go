package controller

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func BenchmarkIndexHandler(b *testing.B) {
	// log.SetOutput(ioutil.Discard)
	for i := 0; i < b.N; i++ {

		r, err := http.NewRequest("GET", "/", nil)
		if err != nil {
			b.Fatal(err)
		}
		w := httptest.NewRecorder()
		handler := http.HandlerFunc(IndexHandler)

		handler.ServeHTTP(w, r)
	}
}
