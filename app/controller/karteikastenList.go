package controller

import (
	"BrainTrain/app/model"
	"BrainTrain/template"
	"log"
	"net/http"
)

func KarteiListHandler(w http.ResponseWriter, r *http.Request) {
	kategorien := model.CategoryTitles()
	var nestedList []model.KarteiCategory

	for _, kat := range kategorien {
		karteien, err := model.KarteienGetByCat(kat)
		if err != nil {
			log.Println("[/kartei/list][KarteienGetByCat(", kat.Id, ")] failed:", err.Error())
		}

		nestedList = append(nestedList, model.KarteiCategory{
			Category:     kat.Title,
			CategoryId:   kat.Id,
			ShowExtended: false,
			Karteien:     karteien,
		})
	}

	karteiList(w, r, false, "Alle Karteikästen", "karteien", nestedList)
}

func KarteiMineListHandler(w http.ResponseWriter, r *http.Request) {

	me := model.User{Id: model.UserGetId(r)}

	myKarteien, err := model.KarteienGetByOwner(me)
	if err != nil {
		log.Println("[/kartei/list][KarteienGetByOwner(", me, ")] failed:", err.Error())
	}

	nestedList := []model.KarteiCategory{
		model.KarteiCategory{
			Category:     "Selbst erstellte Karteikästen",
			ShowExtended: true,
			Karteien:     myKarteien,
		},
		model.KarteiCategory{
			Category:     "Gelernte Karteikarten anderer Nutzer",
			ShowExtended: false,
			Karteien:     nil, // Query to complex, TODO
		},
	}

	karteiList(w, r, true, "Meine Karteikästen", "karteien-mine", nestedList)
}

func karteiList(w http.ResponseWriter, r *http.Request, mine bool, title string, navActive string, schrank []model.KarteiCategory) {
	type Data struct {
		Head    template.Head
		Mine    bool
		Schrank []model.KarteiCategory
	}

	cKarteienPublic := make(chan int)
	go getKarteienCount(cKarteienPublic, true)

	data := Data{
		Head: template.Head{
			CountKarteien:  <-cKarteienPublic,
			LoggedInStatus: model.IsUserLoggedIn(r),
			User:           model.UserGetById(model.UserGetId(r)),
			Title:          title,
			NavActive:      navActive,
		},
		Mine:    mine,
		Schrank: schrank,
	}

	template.Render(w, "kartei/list.html", data)
}
