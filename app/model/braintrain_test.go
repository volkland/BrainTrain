package model

import (
	"fmt"
	"net/http"
)

func ExampleGetId() {
	r, _ := http.NewRequest("GET", "http://example.com/controller/action/abcdef", nil)
	id := GetId(r, "/controller/action")
	fmt.Println(id)

	r2, _ := http.NewRequest("GET", "http://example.com/controller/action-without-id", nil)
	id2 := GetId(r2, "/controller/action-without-id")
	fmt.Println(id2)

	// Output:
	// abcdef
	//
}
